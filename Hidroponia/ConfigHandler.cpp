#include "ConfigHandler.h"

const int READ_FULL_CONFIG = 1;
const int WRITE_FULL_CONFIG = 2;
const int READ_WIFI_CONFIG = 3;
const int WRITE_WIFI_CONFIG = 4;
const int READ_MQTT_CONFIG = 5;
const int WRITE_MQTT_CONFIG = 6;
const int READ_MQTT_TOPICS = 7;
const int WRITE_MQTT_TOPICS = 8;
const int READ_PH_CALIBRATIONS = 9;
const int WRITE_PH_CALIBRATIONS = 10;
const int READ_TDS_CALIBRATIONS = 11;
const int WRITE_TDS_CALIBRATIONS = 12;
const int READ_WATER_TEMP_CALIBRATIONS = 13;
const int WRITE_WATER_TEMP_CALIBRATIONS = 14;
const int READ_SENSORS = 15;

bool ReceivedData(HardwareSerial &outSerial, Config &netConfig)
{
  StaticJsonDocument<DOC_SIZE> doc;
  ReadBufferingStream bufferedStrm(outSerial, 64);
  DeserializationError error = deserializeJson(doc, bufferedStrm);

  if (error)
  {
    Serial.print(F("[SERIAL]    : FAIL Json parser: "));
    Serial.println(error.f_str());
    return false;
  }
  else
  {
    Serial.println(F("[SERIAL]    : JSON parsed succesfully"));
    int command = doc["command"];

    switch (command)
    {
    case WRITE_WIFI_CONFIG:
      strlcpy(netConfig.wifi_ssid, doc["wifi_ssid"], sizeof(netConfig.wifi_ssid));
      strlcpy(netConfig.wifi_pass, doc["wifi_pass"], sizeof(netConfig.wifi_pass));
      return true;
      break;

    case WRITE_MQTT_CONFIG:
      strlcpy(netConfig.mqtt_user, doc["mqtt_user"], sizeof(netConfig.mqtt_user));
      strlcpy(netConfig.mqtt_pass, doc["mqtt_pass"], sizeof(netConfig.mqtt_pass));
      strlcpy(netConfig.mqtt_server, doc["mqtt_server"], sizeof(netConfig.mqtt_server));
      netConfig.mqtt_port = doc["mqtt_port"];
      return true;
      break;

    case WRITE_MQTT_TOPICS:
      strlcpy(netConfig.topic_humidity, doc["topic_humidity"], sizeof(netConfig.topic_humidity));
      strlcpy(netConfig.topic_water_temp, doc["topic_water_temp"], sizeof(netConfig.topic_water_temp));
      strlcpy(netConfig.topic_ambient_temp, doc["topic_ambient_temp"], sizeof(netConfig.topic_ambient_temp));
      strlcpy(netConfig.topic_ph, doc["topic_ph"], sizeof(netConfig.topic_ph));
      strlcpy(netConfig.topic_tds, doc["topic_tds"], sizeof(netConfig.topic_tds));
      strlcpy(netConfig.topic_solution_a, doc["topic_solution_a"], sizeof(netConfig.topic_solution_a));
      strlcpy(netConfig.topic_solution_b, doc["topic_solution_b"], sizeof(netConfig.topic_solution_b));
      // strlcpy(netConfig.topic_solution_c, doc["topic_solution_c"], sizeof(netConfig.topic_solution_c));
      // strlcpy(netConfig.topic_solution_d, doc["topic_solution_d"], sizeof(netConfig.topic_solution_d));
      // strlcpy(netConfig.topic_ph_rise, doc["topic_ph_rise"], sizeof(netConfig.topic_ph_rise));
      // strlcpy(netConfig.topic_ph_decrease, doc["topic_ph_decrease"], sizeof(netConfig.topic_ph_decrease));
      return true;
      break;

    case READ_WIFI_CONFIG:
      doc.clear();
      doc["command"] = command;
      doc["eeprom_counter"] = netConfig.eeprom_counter;
      doc["wifi_ssid"] = netConfig.wifi_ssid;
      doc["wifi_pass"] = netConfig.wifi_pass;
      Serial.println(F("[SERIAL]    : Sending wifi info"));
      serializeJson(doc, outSerial);
      outSerial.println(F(""));
      break;

    case READ_MQTT_CONFIG:
      doc.clear();
      doc["command"] = command;
      doc["eeprom_counter"] = netConfig.eeprom_counter;
      doc["mqtt_user"] = netConfig.mqtt_user;
      doc["mqtt_pass"] = netConfig.mqtt_pass;
      doc["mqtt_port"] = netConfig.mqtt_port;
      doc["mqtt_server"] = netConfig.mqtt_server;
      Serial.println(F("[SERIAL]    : Sending mqtt config info"));
      serializeJson(doc, outSerial);
      outSerial.println(F(""));
      break;

    case READ_MQTT_TOPICS:
      doc.clear();
      doc["command"] = command;
      doc["eeprom_counter"] = netConfig.eeprom_counter;
      doc["topic_humidity"] = netConfig.topic_humidity;
      doc["topic_water_temp"] = netConfig.topic_water_temp;
      doc["topic_ambient_temp"] = netConfig.topic_ambient_temp;
      doc["topic_ph"] = netConfig.topic_ph;
      doc["topic_tds"] = netConfig.topic_tds;
      doc["topic_solution_a"] = netConfig.topic_solution_a;
      doc["topic_solution_b"] = netConfig.topic_solution_b;
      // doc["topic_solution_c"] = netConfig.topic_solution_c;
      // doc["topic_solution_d"] = netConfig.topic_solution_d;
      // doc["topic_ph_rise"] = netConfig.topic_ph_rise;
      // doc["topic_ph_decrease"] = netConfig.topic_ph_decrease;
      Serial.println(F("[SERIAL]    : Sending topics info"));
      serializeJson(doc, outSerial);
      outSerial.println(F(""));
      break;

    case READ_PH_CALIBRATIONS:
      doc.clear();
      doc["command"] = command;
      doc["eeprom_counter"] = netConfig.eeprom_counter;
      doc["ph_slope"] = netConfig.ph_slope;
      doc["ph_intercept"] = netConfig.ph_intercept;
      Serial.println(F("[SERIAL]    : Sending pH calibration info"));
      serializeJson(doc, outSerial);
      outSerial.println(F(""));
      break;

    case READ_TDS_CALIBRATIONS:
      doc.clear();
      doc["command"] = command;
      doc["eeprom_counter"] = netConfig.eeprom_counter;
      doc["tds_slope"] = netConfig.tds_slope;
      doc["tds_intercept"] = netConfig.tds_intercept;
      Serial.println(F("[SERIAL]    : Sending TDS calibration info"));
      serializeJson(doc, outSerial);
      outSerial.println(F(""));
      break;

    case READ_WATER_TEMP_CALIBRATIONS:
      doc.clear();
      doc["command"] = command;
      doc["eeprom_counter"] = netConfig.eeprom_counter;
      doc["water_slope"] = netConfig.water_slope;
      doc["water_intercept"] = netConfig.water_intercept;
      Serial.println(F("[SERIAL]    : Sending water calibration info"));
      serializeJson(doc, outSerial);
      outSerial.println(F(""));
      break;

    case WRITE_PH_CALIBRATIONS:
      netConfig.ph_slope = doc["ph_slope"];
      netConfig.ph_intercept = doc["ph_intercept"];
      return true;
      break;

    case WRITE_TDS_CALIBRATIONS:
      netConfig.tds_slope = doc["tds_slope"];
      netConfig.tds_intercept = doc["tds_intercept"];
      return true;
      break;

    case WRITE_WATER_TEMP_CALIBRATIONS:
      netConfig.water_slope = doc["water_slope"];
      netConfig.water_intercept = doc["water_intercept"];
      return true;
      break;

    case READ_SENSORS:
      break;

    default:
      Serial.println(F("[SERIAL]    : Unknown Read or Write command"));
      break;
    }

    // PrintConfig(netConfig);
    return false;
  }
}

void WriteEEPROM(Config &netConfig)
{
  //   To WRITE a JSON document to EEPROM, use:
  StaticJsonDocument<DOC_SIZE> doc;
  EepromStream eepromStream(START_ADDRESS, DOC_SIZE);
  doc["eeprom_counter"] = netConfig.eeprom_counter + 1;
  doc["wifi_ssid"] = netConfig.wifi_ssid;
  doc["wifi_pass"] = netConfig.wifi_pass;
  doc["mqtt_user"] = netConfig.mqtt_user;
  doc["mqtt_pass"] = netConfig.mqtt_pass;
  doc["mqtt_port"] = netConfig.mqtt_port;
  doc["mqtt_server"] = netConfig.mqtt_server;
  doc["topic_humidity"] = netConfig.topic_humidity;
  doc["topic_water_temp"] = netConfig.topic_water_temp;
  doc["topic_ambient_temp"] = netConfig.topic_ambient_temp;
  doc["topic_ph"] = netConfig.topic_ph;
  doc["topic_tds"] = netConfig.topic_tds;
  doc["topic_solution_a"] = netConfig.topic_solution_a;
  doc["topic_solution_b"] = netConfig.topic_solution_b;
  // doc["topic_solution_c"] = netConfig.topic_solution_c;
  // doc["topic_solution_d"] = netConfig.topic_solution_d;
  // doc["topic_ph_rise"] = netConfig.topic_ph_rise;
  // doc["topic_ph_decrease"] = netConfig.topic_ph_decrease;
  doc["ph_slope"] = netConfig.ph_slope;
  doc["ph_intercept"] = netConfig.ph_intercept;
  doc["tds_slope"] = netConfig.tds_slope;
  doc["tds_intercept"] = netConfig.tds_intercept;
  doc["water_slope"] = netConfig.water_slope;
  doc["water_intercept"] = netConfig.water_intercept;

  serializeJson(doc, eepromStream);
}

bool ReadEEPROM(Config &netConfig)
{
  // To READ a JSON document to EEPROM, use:
  StaticJsonDocument<DOC_SIZE> doc;
  EepromStream eepromStream(START_ADDRESS, DOC_SIZE);
  DeserializationError error = deserializeJson(doc, eepromStream);

  // Test if parsing succeeds.
  if (error)
  {
    Serial.print(F("[EEPROM]    : JSON parsed failed with error: "));
    Serial.println(error.f_str());
    return false;
  }
  else
  {
    Serial.println(F("[EEPROM]    : JSON parsed Succesfully"));
    netConfig.eeprom_counter = doc["eeprom_counter"];
    netConfig.mqtt_port = doc["mqtt_port"];
    netConfig.ph_slope = doc["ph_slope"];
    netConfig.ph_intercept = doc["ph_intercept"];
    netConfig.tds_slope = doc["tds_slope"];
    netConfig.tds_intercept = doc["tds_intercept"];
    netConfig.water_slope = doc["water_slope"];
    netConfig.water_intercept = doc["water_intercept"];
    strlcpy(netConfig.wifi_ssid, doc["wifi_ssid"], sizeof(netConfig.wifi_ssid));
    strlcpy(netConfig.wifi_pass, doc["wifi_pass"], sizeof(netConfig.wifi_pass));
    strlcpy(netConfig.mqtt_user, doc["mqtt_user"], sizeof(netConfig.mqtt_user));
    strlcpy(netConfig.mqtt_pass, doc["mqtt_pass"], sizeof(netConfig.mqtt_pass));
    strlcpy(netConfig.mqtt_server, doc["mqtt_server"], sizeof(netConfig.mqtt_server));
    strlcpy(netConfig.topic_humidity, doc["topic_humidity"], sizeof(netConfig.topic_humidity));
    strlcpy(netConfig.topic_water_temp, doc["topic_water_temp"], sizeof(netConfig.topic_water_temp));
    strlcpy(netConfig.topic_ambient_temp, doc["topic_ambient_temp"], sizeof(netConfig.topic_ambient_temp));
    strlcpy(netConfig.topic_ph, doc["topic_ph"], sizeof(netConfig.topic_ph));
    strlcpy(netConfig.topic_tds, doc["topic_tds"], sizeof(netConfig.topic_tds));
    strlcpy(netConfig.topic_solution_a, doc["topic_solution_a"], sizeof(netConfig.topic_solution_a));
    strlcpy(netConfig.topic_solution_b, doc["topic_solution_b"], sizeof(netConfig.topic_solution_b));
    // strlcpy(netConfig.topic_solution_c, doc["topic_solution_c"], sizeof(netConfig.topic_solution_c));
    // strlcpy(netConfig.topic_solution_d, doc["topic_solution_d"], sizeof(netConfig.topic_solution_d));
    // strlcpy(netConfig.topic_ph_rise, doc["topic_ph_rise"], sizeof(netConfig.topic_ph_rise));
    // strlcpy(netConfig.topic_ph_decrease, doc["topic_ph_decrease"], sizeof(netConfig.topic_ph_decrease));
    return true;
  }
}

void PrintConfig(Config &netConfig)
{
  Serial.println(netConfig.eeprom_counter);
  Serial.println(netConfig.wifi_ssid);
  Serial.println(netConfig.wifi_pass);
  Serial.println(netConfig.mqtt_user);
  Serial.println(netConfig.mqtt_pass);
  Serial.println(netConfig.mqtt_port);
  Serial.println(netConfig.mqtt_server);
  Serial.println(netConfig.topic_humidity);
  Serial.println(netConfig.topic_solution_a);
  Serial.println(netConfig.topic_solution_b);
  // Serial.println(netConfig.topic_solution_c);
  // Serial.println(netConfig.topic_solution_d);
  // Serial.println(netConfig.topic_ph_rise);
  // Serial.println(netConfig.topic_ph_decrease);
  Serial.println(netConfig.ph_slope);
  Serial.println(netConfig.ph_intercept);
  Serial.println(netConfig.tds_slope);
  Serial.println(netConfig.tds_intercept);
  Serial.println(netConfig.water_slope);
  Serial.println(netConfig.water_intercept);
}
