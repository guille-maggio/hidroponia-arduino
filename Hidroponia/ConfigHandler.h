#ifndef CONFIGHANDLER_H
#define CONFIGHANDLER_H
#include <ArduinoJson.h>
#include <StreamUtils.h>

const int STRING_LENGTH = 25;
const int DOC_SIZE = 600;
const int START_ADDRESS = 0;
const int EEPROM_COUNTER = 0;
const int MQTT_PORT = 1883;

struct Config
{
  unsigned int eeprom_counter = EEPROM_COUNTER;
  char wifi_ssid[STRING_LENGTH] = "";
  char wifi_pass[STRING_LENGTH] = "";
  char mqtt_user[STRING_LENGTH] = "";
  char mqtt_pass[STRING_LENGTH] = "";
  char mqtt_server[STRING_LENGTH] = "";
  char topic_humidity[STRING_LENGTH] = "";
  char topic_ambient_temp[STRING_LENGTH] = "";
  char topic_water_temp[STRING_LENGTH] = "";
  char topic_ph[STRING_LENGTH] = "";
  char topic_tds[STRING_LENGTH] = "";
  char topic_solution_a[STRING_LENGTH] = "";
  char topic_solution_b[STRING_LENGTH] = "";
  // char topic_solution_c[STRING_LENGTH] = "";
  // char topic_solution_d[STRING_LENGTH] = "";
  // char topic_ph_rise[STRING_LENGTH] = "";
  // char topic_ph_decrease[STRING_LENGTH] = "";
  int mqtt_port = MQTT_PORT;
  float ph_slope = 1;
  float ph_intercept = 1;
  float tds_slope = 1;
  float tds_intercept = 1;
  float water_slope = 1;
  float water_intercept = 1;
};

struct Sensors
{
  float hum = 0;
  float ambient_temp = 0;
  float water_temp = 0;
  float tds = 0;
  float ph = 0;
  bool relay01 = false;
  bool relay02 = false;
  bool relay03 = false;
  bool relay04 = false;
  bool relay05 = false;
  bool relay06 = false;
};

void WriteEEPROM(Config &netConfig);
bool ReadEEPROM(Config &netConfig);
void PrintConfig(Config &netConfig);
bool ReceivedData(HardwareSerial &btSerial, Config &netConfig);
void SendData(HardwareSerial &btSerial, Config &netConfig, int command);

#endif
