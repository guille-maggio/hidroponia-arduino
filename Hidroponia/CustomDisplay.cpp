#include "CustomDisplay.h"

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

void InitDisplay()
{
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
  {
    // Address 0x3D for 128x64
    Serial.println(F("[DISPLAY]   : SSD1306 allocation failed"));
  }
  else
  {
    Serial.println(F("[DISPLAY]   : SSD1306 Initialized"));
  }
}

void PrintDisplay(Sensors &sensors)
{

  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);

  display.print(F("Tem: "));
  display.println(sensors.water_temp);

  display.print(F("Hum: "));
  display.println(sensors.hum);

  display.print(F("PH : "));
  display.println(sensors.ph);

  display.print(F("TDS: "));
  display.println(sensors.tds);

  // display.print(F("Out: "));
  // if (sensors.relay01){
  //     display.println(F("ON"));
  //     }
  // else display.println(F("OFF"));

  display.display();
}

void PrintError()
{
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);

  display.println(F("    "));
  display.println(F(" ERROR  "));
  display.println(F(" Parsing"));
  display.println(F(" Config "));

  display.display();
}

void PrintSuccess()
{
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);

  display.println(F("    "));
  display.println(F(" SUCCESS"));
  display.println(F(" Parsing"));
  display.println(F(" Config "));

  display.display();
}