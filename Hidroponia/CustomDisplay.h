#ifndef CUSTOMDISPLAY_H
#define CUSTOMDISPLAY_H
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "ConfigHandler.h"

const int SCREEN_WIDTH = 128; // OLED display width, in pixels
const int SCREEN_HEIGHT = 64; // OLED display height, in pixels

void InitDisplay();
void PrintDisplay(Sensors &sensors);
void PrintError();
void PrintSuccess();

#endif
