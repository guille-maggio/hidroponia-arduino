
#ifndef DHTTOOLS_H
#define DHTTOOLS_H
#include <DHTStable.h>

#define DHTPIN 8

DHTStable DHT;


void InitDHT()
{
  // dht.begin(); 
}

void ReadDHT(){
    int state = DHT.read22(DHTPIN);
    switch (state)
    {
    case DHTLIB_OK:
        Serial.println("[DHT]       : OK");
        break;
    case DHTLIB_ERROR_CHECKSUM:
        Serial.println("[DHT]       : Checksum error");
        break;
    case DHTLIB_ERROR_TIMEOUT:
        Serial.println("[DHT]       : Time out error");
        break;
    default:
        Serial.println("[DHT]       : Unknown error");
        break;
    }
   }

float ReadTemp()
{
  float temp = DHT.getTemperature();
  return temp;
}


float ReadHum()
{
  float hum = DHT.getHumidity();
  return hum;
}

#endif
