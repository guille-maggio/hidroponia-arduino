#include "SoftTimers.h"
#include "ConfigHandler.h"
#include "Bluetooth.h"
#include "WiFiTools.h"
#include "MQTTTools.h"
#include "Relays.h"
#include "CustomDisplay.h"
#include "DHTTools.h"
#include "PH.h"
#include "TDS.h"
#include "ResetDevices.h"
#include <MemoryFree.h>
// #include "avr8-stub.h"

// DEFINE TIMERS
SoftTimer wifiLoop;
SoftTimer mqttLoop;
SoftTimer mqttReconnectLoop;
SoftTimer publishLoop;
SoftTimer tdsLoop;
SoftTimer sensorsLoop;
SoftTimer displayLoop;
SoftTimer resetLoop;

//*************** CONFIG DEFINITIONS ***********************//
Config config;
int command;
const int RESET_TIME = 10;  //10 minutes
//*************** SERIAL DEFINITIONS ***********************//
HardwareSerial &wifiSerial = Serial1;
HardwareSerial &btSerial = Serial2;

//*************** RELAY DEFINITIONS ************************//
const int PIN_RELAY01 = 3;
const int PIN_RELAY02 = 4;
const int PIN_RELAY03 = 5;
const int PIN_RELAY04 = 6;
const int PIN_RELAY05 = 9;
const int PIN_RELAY06 = 10;
const int PIN_PH = A2;
const int PIN_WATER_TEMP = A0;
const int PIN_TDS = A3;
const int PIN_TDS_VCC = A4;
const int PIN_TDS_GND = A5;
const int PIN_ARDUINO_RESET = A6;
const int PIN_WIFI_RESET = A7;

// Relay relay01 = Relay(PIN_RELAY01, NC);
// Relay relay02 = Relay(PIN_RELAY02, NC);
// Relay relay03 = Relay(PIN_RELAY03, NC);
// Relay relay04 = Relay(PIN_RELAY04, NC);
// Relay relay05 = Relay(PIN_RELAY05, NC);
// Relay relay06 = Relay(PIN_RELAY06, NC);
PHSensor phSensor01 = PHSensor(PIN_PH, PIN_WATER_TEMP);
TDSSensor tdsSensor01 = TDSSensor(PIN_TDS, PIN_TDS_VCC, PIN_TDS_GND);

//*************** SENSOR VARIABLES *************************//
Sensors sensors;
int runningTimeCounter = 0;
//*************** GLOBAL FUNCTIONS PROTOTYPES **************//
void ReadSensors();
void ReadRelays();
void SubscribeTopics();
void ReceiveTopics(char *topic, byte *payload, unsigned int len);
void UpdateRelayFromPayload(Relay &relay, char *charPay);
void UpdateComplementaryRelays(Relay &mainRelay, Relay &secRelay, char *charPay);

//*************** SETUP  ************************************//
void setup()
{
  // debug_init();
  Serial.begin(115200);

  // relay01.Init();
  // relay02.Init();
  // relay03.Init();
  // relay04.Init();
  // relay05.Init();
  // relay06.Init();
  Serial.println(F("----      Setup Init      ----"));

  ReadEEPROM(config);
  InitBluetooth(btSerial);
  InitReset(PIN_WIFI_RESET, PIN_ARDUINO_RESET);
  InitWiFi(wifiSerial);
  ReconnectWifi(config.wifi_ssid, config.wifi_pass, PIN_WIFI_RESET, PIN_ARDUINO_RESET);
  InitDisplay();
  InitDHT();
  InitMQTT(config.mqtt_server, config.mqtt_port, ReceiveTopics);
  
  phSensor01.Init();
  tdsSensor01.Init();
  phSensor01.CalibratePh(config.ph_slope, config.ph_intercept);
  phSensor01.CalibrateWaterTemp(config.water_slope, config.water_intercept);
  tdsSensor01.Calibrate(config.tds_slope, config.tds_intercept);

  wifiLoop.setTimeOutTime(60000); // every 60 seconds.
  wifiLoop.reset();

  mqttLoop.setTimeOutTime(10); // every 10 mili seconds.
  mqttLoop.reset();

  mqttReconnectLoop.setTimeOutTime(15000); // every 15 seconds.
  mqttReconnectLoop.reset();

  publishLoop.setTimeOutTime(20000); // every 20 seconds.
  publishLoop.reset();

  tdsLoop.setTimeOutTime(2000); // every 2 seconds.
  tdsLoop.reset();

  sensorsLoop.setTimeOutTime(10000); // every 10 seconds.
  sensorsLoop.reset();

  displayLoop.setTimeOutTime(10000); // every 10 seconds.
  displayLoop.reset();

  resetLoop.setTimeOutTime(5*60000); // every 5 minutes.
  resetLoop.reset();
}

//***************MAIN LOOP ************************//

void loop()
{

  // Bluetooth Serial Communication
  if (btSerial.available() > 1)
  {
    Serial.println(F("----  Receiving Bluetooth ----"));
    if (ReceivedData(btSerial, config))
    {
      WriteEEPROM(config);
      if (ReadEEPROM(config)) {
        PrintSuccess();
      }
      else {
        PrintError();
      }
      // PrintConfig(config);
      ReconnectWifi(config.wifi_ssid, config.wifi_pass, PIN_WIFI_RESET, PIN_ARDUINO_RESET);
      ReconnectMQTT(config.mqtt_server, config.mqtt_port, config.mqtt_user, config.mqtt_pass);
      SubscribeTopics();
      phSensor01.CalibratePh(config.ph_slope, config.ph_intercept);
      phSensor01.CalibrateWaterTemp(config.water_slope, config.water_intercept);
      tdsSensor01.Calibrate(config.tds_slope, config.tds_intercept);      
    }
  }

  // USB Serial Communication
  if (Serial.available() > 1)
  {
    Serial.println(F("----     Receiving USB    ----"));
    if (ReceivedData(Serial, config))
    {
      WriteEEPROM(config);
      if (ReadEEPROM(config)) {
        PrintSuccess();
      }
      else {
        PrintError();
      }      // PrintConfig(config);
      ReconnectWifi(config.wifi_ssid, config.wifi_pass, PIN_WIFI_RESET, PIN_ARDUINO_RESET);
      ReconnectMQTT(config.mqtt_server, config.mqtt_port, config.mqtt_user, config.mqtt_pass);
      SubscribeTopics();
      phSensor01.CalibratePh(config.ph_slope, config.ph_intercept);
      phSensor01.CalibrateWaterTemp(config.water_slope, config.water_intercept);
      tdsSensor01.Calibrate(config.tds_slope, config.tds_intercept);
    }
  }

  // WiFi Reconnection
  if (wifiLoop.hasTimedOut())
  { 
    Serial.println(F("----  WiFi Reconnection   ----"));
    // Print Running Time
    Serial.print(F("[RUNNING]   : "));
    Serial.print(runningTimeCounter);
    Serial.println(F(" minutes"));

    runningTimeCounter++;

    if (WiFi.status() != WL_CONNECTED)
    {
      ReconnectWifi(config.wifi_ssid, config.wifi_pass, PIN_WIFI_RESET, PIN_ARDUINO_RESET);
    }
    wifiLoop.reset(); // next refresh in 30 seconds
  }

  // MQTT loop every 10ms needed to MQTT work properly
  if (mqttLoop.hasTimedOut())
  {
    MQTTLoop();
    mqttLoop.reset();
  }

  // Try to Reconnect MQTT after 15 seconds
  if (mqttReconnectLoop.hasTimedOut())
  {
    Serial.println(F("----  MQTT Reconnection   ----"));
    if (!MQTTConnected() && WiFi.status() == WL_CONNECTED)
    {
      ReconnectMQTT(config.mqtt_server, config.mqtt_port, config.mqtt_user, config.mqtt_pass);
    }
    // if (MQTTConnected()) SubscribeTopics();
    mqttReconnectLoop.reset();
  }

  // Read TDS sensor and then turn it off so it doesn't affect pH reading
  // this loop is only reseted after sensorsLoop has finished
  if (tdsLoop.hasTimedOut() && tdsSensor01.IsOn())
  {
    Serial.println(F("----     TDS Reading      ----"));
    sensors.tds = tdsSensor01.Read();
    tdsSensor01.Off();
  }

  // Read all sensors and relays except TDS
  if (sensorsLoop.hasTimedOut())
  {
    Serial.println(F("----   Sensors Reading    ----"));
    ReadSensors();
    // ReadRelays();
    tdsSensor01.On();
    tdsLoop.reset();
    sensorsLoop.reset();
  }

  // Publish all values into corresponding MQTT topics
  if (publishLoop.hasTimedOut())
  {
    Serial.println(F("----   Publishing Topics  ----"));
    if (MQTTConnected())
    {
      PublishTopics(config, sensors);
    }
    publishLoop.reset();
  }

  // Print values in OLED display
  if (displayLoop.hasTimedOut())
  {
    Serial.println(F("----   Printing Display   ----"));
    PrintDisplay(sensors);
    displayLoop.reset();
  }

    // Reset Arduino and Wifi
  if (runningTimeCounter > RESET_TIME)
  {
    FullReset(PIN_WIFI_RESET, PIN_ARDUINO_RESET);
    // resetLoop.reset();
  }
}

//*************** GLOBAL FUNCTIONS *************************//

void ReadSensors()
{
  ReadDHT();
  sensors.ph = phSensor01.ReadPh();
  sensors.water_temp = phSensor01.ReadWaterTemp();
  sensors.ambient_temp = ReadTemp();
  sensors.hum = ReadHum();
}

void ReadRelays()
{
  // sensors.relay01 = relay01.GetState();
  // sensors.relay02 = relay02.GetState();
  // sensors.relay03 = relay03.GetState();
  // sensors.relay04 = relay04.GetState();
  // sensors.relay05 = relay05.GetState();
  // sensors.relay06 = relay06.GetState();
}

void SubscribeTopics()
{
  Serial.println(F("[MQTT]      : SUBSCRIBING"));
  SubscribeTopic(config.topic_solution_a);
  SubscribeTopic(config.topic_solution_b);
  // SubscribeTopic(config.topic_solution_c);
  // SubscribeTopic(config.topic_solution_d);
  // SubscribeTopic(config.topic_ph_rise);
  // SubscribeTopic(config.topic_ph_decrease);
}

void ReceiveTopics(char *topic, byte *payload, unsigned int len)
{
  char charPayload[len + 1];
  for (unsigned int i = 0; i < len; i++)
  {
    charPayload[i] = (char)payload[i];
  }
  charPayload[len] = '\0';

  Serial.print(F("[MQTT]      : Received "));
  Serial.print(topic);
  Serial.print(F(" "));
  Serial.println(charPayload);

  // if (strcmp(topic, config.topic_solution_a) == 0)
  // {
  //   UpdateRelayFromPayload(relay01, charPayload);
  // }
  // else if (strcmp(topic, config.topic_solution_b) == 0)
  // {
  //   UpdateRelayFromPayload(relay02, charPayload);
  // }
  // else if (strcmp(topic, config.topic_solution_c) == 0)
  // {
  //   UpdateRelayFromPayload(relay03, charPayload);
  // }
  // else if (strcmp(topic, config.topic_solution_d) == 0)
  // {
  //   UpdateRelayFromPayload(relay04, charPayload);
  // }
  // else if (strcmp(topic, config.topic_ph_rise) == 0)
  // {
  //   UpdateComplementaryRelays(relay05, relay06, charPayload);
  // }
  // else if (strcmp(topic, config.topic_ph_decrease) == 0)
  // {
  //   UpdateComplementaryRelays(relay06, relay05, charPayload);
  // }
  // ReadRelays();

  Serial.print(F("[FREEMEMORY]: Receive Topic Space left: "));
  Serial.println(freeMemory());
  // Serial.println(F("-----------------------"));
}

void UpdateRelayFromPayload(Relay &relay, char *charPay)
{
  if (strcmp(charPay, "ON") == 0)
  {
    relay.On();
  }
  else if (strcmp(charPay, "OFF") == 0)
  {
    relay.Off();
  }
}

void UpdateComplementaryRelays(Relay &mainRelay, Relay &secRelay, char *charPay)
{
  if (strcmp(charPay, "ON") == 0)
  {
    mainRelay.On();
    secRelay.Off();
  }
  else if (strcmp(charPay, "OFF") == 0)
  {
    mainRelay.Off();
  }
}
