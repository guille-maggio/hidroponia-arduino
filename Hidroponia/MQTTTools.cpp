#include "MQTTTools.h"

WiFiClient espClient;
PubSubClient client(espClient);

void InitMQTT(char *server, int port, void (*func)(char *, byte *, unsigned int))
{
  client.setServer(server, port);
  client.setCallback(func);
}

void MQTTLoop()
{
  client.loop();
}

bool MQTTConnected()
{
  printMQTTStatus(client.state());
  if (client.state() >= CONNECTED)
  {
    return true;
  }
  else
    return false;
}

void ReconnectMQTT(char *server, int port)
{
  Serial.print(F("[MQTT]      : Connecting to "));
  Serial.println(server);
  client.setServer(server, port);
  client.setKeepAlive(90);

  // String clientId = "HidropClient-" + String(random(0xffff), HEX);
  // if (client.connect(clientId.c_str()))

  if (client.connect("HidropClient"))
  {
    Serial.println(F("[MQTT]      : Connected"));
  }
  else
  {
    Serial.println(F("[MQTT]      : Failed to connect"));
    printMQTTStatus(client.state());
  }
}

void ReconnectMQTT(char *server, int port, char *user, char *pass)
{
  Serial.print(F("[MQTT]      : Connecting to "));
  Serial.println(server);
  client.setServer(server, port);
  client.setKeepAlive(90);

  if ((user == NULL) || (user[0] == '\0'))
  {
    client.connect("HidropClient");
  }
  else
  {
    client.connect("HidropClient", user, pass);
  }
  printMQTTStatus(client.state());
}

void SubscribeTopic(char *topic)
{
  client.subscribe(topic, SUBQOS);
  Serial.print(F("[MQTT]      : Subscribed to topic: "));
  Serial.println(topic);
}

void PublishTopics(Config &netConfig, Sensors &sensors)
{

  char ambientTemp[BUFFER_DTOSTRF];
  dtostrf(sensors.ambient_temp, 6, 2, ambientTemp);
  Serial.print(F("[SENSOR]    : AmbientTemp "));
  Serial.println(ambientTemp);

  char waterTemp[BUFFER_DTOSTRF];
  dtostrf(sensors.water_temp, 6, 2, waterTemp);
  Serial.print(F("[SENSOR]    : Temperatura "));
  Serial.println(waterTemp);

  char humidity[BUFFER_DTOSTRF];
  dtostrf(sensors.hum, 6, 2, humidity);
  Serial.print(F("[SENSOR]    : Humedad     "));
  Serial.println(humidity);

  char ph[BUFFER_DTOSTRF];
  dtostrf(sensors.ph, 6, 2, ph);
  Serial.print(F("[SENSOR]    : PH          "));
  Serial.println(ph);

  char tds[BUFFER_DTOSTRF];
  dtostrf(sensors.tds, 6, 2, tds);
  Serial.print(F("[SENSOR]    : TDS         "));
  Serial.println(tds);

  client.publish(netConfig.topic_water_temp, waterTemp, PUBQOS);
  client.publish(netConfig.topic_ambient_temp, ambientTemp, PUBQOS);
  client.publish(netConfig.topic_humidity, humidity, PUBQOS);
  client.publish(netConfig.topic_ph, ph, PUBQOS);
  client.publish(netConfig.topic_tds, tds, PUBQOS);

  // char switch01[BUFFER_DTOSTRF];
  // if (sensors.relay01)
  // {
  //   strlcpy(switch01, "ON", sizeof(switch01));
  // }
  // else
  //   strlcpy(switch01, "OFF", sizeof(switch01));

  // char switch02[BUFFER_DTOSTRF];
  // if (sensors.relay02)
  // {
  //   strlcpy(switch02, "ON", sizeof(switch02));
  // }
  // else
  //   strlcpy(switch02, "OFF", sizeof(switch02));

  // char switch03[BUFFER_DTOSTRF];
  // if (sensors.relay03)
  // {
  //   strlcpy(switch03, "ON", sizeof(switch03));
  // }
  // else
  //   strlcpy(switch03, "OFF", sizeof(switch03));

  // char switch04[BUFFER_DTOSTRF];
  // if (sensors.relay04)
  // {
  //   strlcpy(switch04, "ON", sizeof(switch04));
  // }
  // else
  //   strlcpy(switch04, "OFF", sizeof(switch04));

  // char switch05[BUFFER_DTOSTRF];
  // if (sensors.relay05)
  // {
  //   strlcpy(switch05, "ON", sizeof(switch05));
  // }
  // else
  //   strlcpy(switch05, "OFF", sizeof(switch05));

  // char switch06[BUFFER_DTOSTRF];
  // if (sensors.relay06)
  // {
  //   strlcpy(switch06, "ON", sizeof(switch06));
  // }
  // else
  //   strlcpy(switch06, "OFF", sizeof(switch06));

  // client.publish(netConfig.topic_solution_a, switch01, PUBQOS);
  // client.publish(netConfig.topic_solution_b, switch02, PUBQOS);
  // client.publish(netConfig.topic_solution_c, switch03, PUBQOS);
  // client.publish(netConfig.topic_solution_d, switch04, PUBQOS);
  // client.publish(netConfig.topic_ph_rise, switch05, PUBQOS);
  // client.publish(netConfig.topic_ph_decrease, switch06, PUBQOS);
  // Serial.print(F("[FREEMEMORY]: Space left: "));
  // Serial.println(freeMemory());
  // Serial.println(F("[MQTT]      : PUBLISHING"));
  // Serial.println(humidity);
  // Serial.println(ambientTemp);
  // Serial.println(switch01);
  // Serial.println(switch02);
  // Serial.println(switch03);
  // Serial.println(switch04);
  // Serial.println(switch05);
  // Serial.println(switch06);
  // Serial.println(F("-----------------------"));
}

void printMQTTStatus(int stat)
{
  switch (stat)
  {
  case CONNECTION_TIMEOUT:
    Serial.println(F("[MQTT]      : MQTT CONNECTION TIMEOUT"));
    break;
  case CONNECTION_LOST:
    Serial.println(F("[MQTT]      : MQTT CONNECTION LOST"));
    break;
  case CONNECT_FAILED:
    Serial.println(F("[MQTT]      : MQTT CONNECT FAILED"));
    break;
  case DISCONNECTED:
    Serial.println(F("[MQTT]      : MQTT DISCONNECTED"));
    break;
  case CONNECTED:
    Serial.println(F("[MQTT]      : MQTT CONNECTED"));
    break;
  case CONNECT_BAD_PROTOCOL:
    Serial.println(F("[MQTT]      : MQTT CONNECT BAD PROTOCOL"));
    break;
  case CONNECT_BAD_CLIENT_ID:
    Serial.println(F("[MQTT]      : MQTT CONNECT BAD CLIENT_ID"));
    break;
  case CONNECT_UNAVAILABLE:
    Serial.println(F("[MQTT]      : MQTT CONNECT UNAVAILABLE"));
    break;
  case CONNECT_BAD_CREDENTIALS:
    Serial.println(F("[MQTT]      : MQTT CONNECT BAD CREDENTIALS"));
    break;
  case CONNECT_UNAUTHORIZED:
    Serial.println(F("[MQTT]      : MQTT CONNECT UNAUTHORIZED"));
    break;
  default:
    Serial.println(F("[MQTT]      : UNKNOWN"));
    break;
  }
}
