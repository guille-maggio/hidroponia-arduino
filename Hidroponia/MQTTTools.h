#ifndef MQTTTOOLS_H
#define MQTTTOOLS_H
#include <WiFiEspAT.h>
#include <PubSubClient.h>
#include "ConfigHandler.h"
#include "MemoryFree.h"
// #include "avr8-stub.h"

const int PUBQOS = 1;
const int SUBQOS = 1;
const int BUFFER_DTOSTRF = 10;

const int CONNECTION_TIMEOUT = -4;
const int CONNECTION_LOST = -3;
const int CONNECT_FAILED = -2;
const int DISCONNECTED = -1;
const int CONNECTED = 0;
const int CONNECT_BAD_PROTOCOL = 1;
const int CONNECT_BAD_CLIENT_ID = 2;
const int CONNECT_UNAVAILABLE = 3;
const int CONNECT_BAD_CREDENTIALS = 4;
const int CONNECT_UNAUTHORIZED = 5;

// void InitMQTT(char* server, int port);
void InitMQTT(char *server, int port, void (*func)(char *, byte *, unsigned int));
void ReconnectMQTT();
void ReconnectMQTT(char *server, int port);
void ReconnectMQTT(char *server, int port, char *user, char *pass);
void MQTTFlush();

// void PublishTopic(char* topic, bool value);
// void PublishTopic(char* topic, float value);

void PublishTopics(Config &netConfig, Sensors &sensors);
void SubscribeTopic(char *topic);
void printMQTTStatus(int stat);

// void ReceiveTopics(char* topic, byte* payload, unsigned int len);
void MQTTLoop();
bool MQTTConnected();

#endif
