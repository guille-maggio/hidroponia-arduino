#include "PH.h"

PHSensor::PHSensor(int phPin, int tempPin)
{
  sensorPin = phPin;
  sensorTempPin = tempPin;
}

void PHSensor::Init()
{ // slope and intercept default
  mPh = 1;
  hPh = 0;
  mWaterTemp = 1;
  hWaterTemp = 0;
}

void PHSensor::CalibratePh(float slope, float intercept)
{
  mPh = slope;
  hPh = intercept;
}

void PHSensor::CalibrateWaterTemp(float slope, float intercept)
{
  mWaterTemp = slope;
  hWaterTemp = intercept;
}

float PHSensor::ReadPh()
{
  float po = (1023 - analogRead(sensorPin));
  po = po * mPh + hPh;
  // po = po * 0.015 + 2;
  // Serial.print(F("[SENSOR]    : pH "));
  // Serial.println(po, 2);
  return po;
}

float PHSensor::ReadWaterTemp()
{
  float to = (1023 -analogRead(sensorTempPin));
  to = to * mWaterTemp + hWaterTemp;
  // Serial.print(F("[SENSOR]    : WaterTemp "));
  // Serial.println(to, 2);
  return to;
}