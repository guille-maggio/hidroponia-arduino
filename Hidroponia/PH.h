#ifndef PH_H
#define PH_H
#include <Arduino.h>

class PHSensor
{
private:
    int sensorPin;
    int sensorTempPin;
    float mPh;
    float hPh;
    float mWaterTemp;
    float hWaterTemp;

public:
    PHSensor(int phPin, int tempPin);
    void Init();
    float ReadPh();
    float ReadWaterTemp();
    void CalibratePh(float slope, float intercept);
    void CalibrateWaterTemp(float slope, float intercept);
};

#endif