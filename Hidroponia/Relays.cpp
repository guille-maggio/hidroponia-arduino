#include "Relays.h"

Relay::Relay(int pin, bool type)
{
  r_type = type;
  r_pin = pin;
  r_state = LOW;
  r_on = LOW;
  r_off = HIGH;
  if (type == NC)
  {
    r_on = LOW;
    r_off = HIGH;
  }
  else if (type == NC)
  {
    r_on = HIGH;
    r_off = LOW;
  }
  r_state = r_off;
}

void Relay::Init()
{
  pinMode(r_pin, OUTPUT);
  digitalWrite(r_pin, r_off);
}
void Relay::On()
{
  r_state = r_on;
  digitalWrite(r_pin, r_on);
}
void Relay::Off()
{
  r_state = r_off;
  digitalWrite(r_pin, r_off);
}
void Relay::Toggle()
{
  r_state = !r_state;
  digitalWrite(r_pin, r_state);
}
unsigned int Relay::GetPin()
{
  return r_pin;
}

bool Relay::GetState()
{
  if (r_state == r_on)
  {
    return true;
  }
  else
    return false;
}
