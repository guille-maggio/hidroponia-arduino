#ifndef RELAYS_H
#define RELAYS_H
#include <Arduino.h>

const int NC = true;
const int NO = false;
class Relay
{
private:
  bool r_state;
  int r_pin;
  int r_type;
  int r_on;
  int r_off;

public:
  Relay(int pin, bool type);
  void Init();
  void On();
  void Off();
  void Toggle();
  unsigned int GetPin();
  bool GetState();
};

#endif
