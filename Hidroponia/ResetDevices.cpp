#include "ResetDevices.h"

void InitReset(int arduinoResetPin, int wifiResetPin){
    pinMode(arduinoResetPin, INPUT); 
    pinMode(wifiResetPin, INPUT); 
}

void FullReset(int wifiResetPin, int arduinoResetPin){
    pinMode(arduinoResetPin, OUTPUT); 
    pinMode(wifiResetPin, OUTPUT); 
    delay(1000);

    Serial.println(F("----   Reseting Arduino   ----"));
    Serial.println(F("[WiFi]      : Resetting ESP-01 and Arduino"));

    digitalWrite(wifiResetPin, LOW);
    delay(1000);
    digitalWrite(arduinoResetPin, LOW);
}

void ArduinoReset(int arduinoResetPin){
    pinMode(arduinoResetPin, OUTPUT); 
    
    Serial.println(F("----   Reseting Arduino   ----"));
    Serial.println(F("[WiFi]      : Resetting Arduino"));
   
    delay(1000);
    digitalWrite(arduinoResetPin, LOW);
}

void WiFiReset(int wifiResetPin){
    pinMode(wifiResetPin, OUTPUT); 

    Serial.println(F("----   Reseting ESP-01    ----"));
    Serial.println(F("[WiFi]      : Resetting ESP-01"));
    
    digitalWrite(wifiResetPin, LOW);
}
