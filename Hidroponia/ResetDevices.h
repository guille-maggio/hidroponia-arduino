#ifndef RESETDEVICES_H
#define RESETDEVICES_H
#include "Arduino.h"

void InitReset(int arduinoResetPin, int wifiResetPin);
void FullReset(int wifiResetPin, int arduinoResetPin);
void ArduinoReset(int arduinoResetPin);
void WiFiReset(int wifiResetPin);

#endif
