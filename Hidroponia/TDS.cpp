#include "TDS.h"

TDSSensor::TDSSensor(int sensor, int vcc, int gnd)
{
    sensorPin = sensor;
    vccPin = vcc;
    gndPin = gnd;
}

float TDSSensor::Read()
{
    float tds = analogRead(sensorPin);
    tds = tds * m + h;
    // Serial.print(F("[SENSOR]    : TDS "));
    // Serial.println(tds, 2);
    return tds;
}

void TDSSensor::Calibrate(float slope, float intercept)
{
    m = slope;
    h = intercept;
}

void TDSSensor::Init()
{
    pinMode(vccPin, OUTPUT);
    pinMode(gndPin, OUTPUT);
    m = 1;
    h = 0;
}

void TDSSensor::On()
{
    state = true;
    digitalWrite(vccPin, LOW);  // Turn on P-Mosfet
    digitalWrite(gndPin, HIGH); // Turn on N-Mosfet
}

void TDSSensor::Off()
{
    state = false;
    digitalWrite(vccPin, HIGH); // Turn off P-Mosfet
    digitalWrite(gndPin, LOW);  // Turn off N-Mosfet
}

bool TDSSensor::IsOn()
{
    return state;
}