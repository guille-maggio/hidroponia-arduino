#ifndef TDS_H
#define TDS_H
#include <Arduino.h>

class TDSSensor
{
private:
    int sensorPin;
    int gndPin;
    int vccPin;
    bool state;
    float m;
    float h;

public:
    TDSSensor(int tdsPin, int vccPin, int gndPin);
    bool IsOn();
    void Init();
    float Read();
    void On();
    void Off();
    void Calibrate(float slope, float intercept);
};

#endif