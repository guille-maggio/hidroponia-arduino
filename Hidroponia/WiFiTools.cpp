#include "WiFiTools.h"

void InitWiFi(HardwareSerial &wifiSerial)
{
  Serial.println(F("[WiFi]      : INIT"));
  // nvs_flash_init();
  wifiSerial.begin(9600);
  WiFi.init(&wifiSerial);
  if (WiFi.status() == WL_NO_MODULE)
  {
    Serial.println(F("[WiFi]      : WiFi shield is not present"));
  }
}

void ReconnectWifi(char *ssid, char *pass, int wifiResetPin, int arduinoResetPin)
{
  //  wifiSerial.listen();
  int stat = WiFi.status();
  printWifiStatus(stat);

  if (stat == WL_NO_SHIELD || stat == WL_NO_MODULE)
  {
    FullReset(wifiResetPin, arduinoResetPin);
  }

  if (stat != WL_CONNECTED)
  {
    Serial.print(F("[WiFi]      : Trying to connect with WPA SSID: "));
    Serial.println(ssid);
    stat = WiFi.begin(ssid, pass);
  }
  printWifiStatus(stat);
}

void printWifiStatus(int stat)
{
  switch (stat)
  {
  case WL_CONNECTED:
    Serial.println(F("[WiFi]      : WL_CONNECTED"));
    break;
  case WL_NO_SHIELD:
    Serial.println(F("[WiFi]      : WL_NO_SHIELD"));
    break;
  case WL_IDLE_STATUS:
    Serial.println(F("[WiFi]      : WL_IDLE_STATUS"));
    break;
  case WL_CONNECT_FAILED:
    Serial.println(F("[WiFi]      : WL_CONNECT_FAILED"));
    break;
  case WL_CONNECTION_LOST:
    Serial.println(F("[WiFi]      : WL_CONNECTION_LOST"));
    break;
  case WL_DISCONNECTED:
    Serial.println(F("[WiFi]      : WL_DISCONNECTED"));
    break;
  default:
    Serial.println(F("[WiFi]      : UNKNOWN"));
    break;

    //    case WL_NO_SSID_AVAIL:
    //      Serial.println(F("[WiFi]: WL_NO_SSID_AVAIL"));
    //      break;
    //    case WL_SCAN_COMPLETED:
    //      Serial.println(F("[WiFi]: WL_SCAN_COMPLETED"));
    //      break;
  }
}
