#ifndef WIFITOOLS_H
#define WIFITOOLS_H
#include <WiFiEspAT.h>
#include "ResetDevices.h"

// #include "nvs_flash.h"

void InitWiFi(HardwareSerial &wifiSerial);
void ReconnectWifi(char *ssid, char *pass, int wifiResetPin, int arduinoResetPin);
void printWifiStatus(int stat);

#endif
