#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <string.h>


SoftwareSerial BTserial(10, 11); // RX | TX

char junk;
const int DOC_SIZE = 400;
const int STRING_LENGTH = 50;
unsigned long prev_millis = 0;
unsigned long curr_millis = 0;
int interval = 5000;

struct Config {
  char wifi_ssid[STRING_LENGTH];
  char wifi_pass[STRING_LENGTH];
  char mqtt_user[STRING_LENGTH];
  char mqtt_pass[STRING_LENGTH];
  char mqtt_server[STRING_LENGTH];
};

Config netConfig;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);     // communication with the host computer
  BTserial.begin(9600);
}

void loop () {
  readBluetooth(DOC_SIZE);
  curr_millis = millis();
  if ( curr_millis - prev_millis > interval) {
    prev_millis = curr_millis;

    // Print values.
    Serial.println(netConfig.wifi_ssid);
    Serial.println(netConfig.wifi_pass);
    Serial.println(netConfig.mqtt_user);
    Serial.println(netConfig.mqtt_pass);
    Serial.println(netConfig.mqtt_server);
  }
}
void readBluetooth(int bufferSize) {
  // Reads Bluetooth serial incomming message and saves it in a char array
  // Includes 1ms delay for serial buffer filling
  //small delay allow input buffer receive one byte
  //too big will cause buffer overflow
  //too small will not receive data
  //1ms its perfect for 9600b/s --> 1 byte every 0.83ms
  //theoretical max message size without overflow ~384 bytes, in practice is arround 490 bytes
  char inputBuffer [bufferSize];

  if (BTserial.available()) {

    int i = 0;
    while (BTserial.available()) {

      delay(1);
      char inChar = (char)BTserial.read(); //read the input
      inputBuffer[i] = inChar;
      i++;
    }
    inputBuffer[i] = '\0';
    Serial.println("-------------------------");
    Serial.println("[BLUETOOTH] INPUT STRING:");
    Serial.println(inputBuffer);
    processJson(inputBuffer);

    // clear the serial buffer if junk info is comming
    while (BTserial.available() > 0) {
      junk = BTserial.read() ;
    }
    clearInputBuffer(inputBuffer);
  }
}

void processJson(char* jsonChars) {
  // Deserialize the JSON document
  StaticJsonDocument<DOC_SIZE> doc;
  DeserializationError error = deserializeJson(doc, jsonChars);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("[JSON PARSER] FAIL: "));
    Serial.println(error.f_str());
  }
  else {
    Serial.println(F("[JSON PARSER] SUCCESS"));
   strlcpy(netConfig.wifi_ssid, doc["wifi_ssid"], sizeof(netConfig.wifi_ssid));
   strlcpy(netConfig.wifi_pass, doc["wifi_pass"], sizeof(netConfig.wifi_pass));
   strlcpy(netConfig.mqtt_user, doc["mqtt_user"], sizeof(netConfig.mqtt_user));
   strlcpy(netConfig.mqtt_pass, doc["mqtt_pass"], sizeof(netConfig.mqtt_pass));
   strlcpy(netConfig.mqtt_server, doc["mqtt_server"], sizeof(netConfig.mqtt_server));


    // Print values.
    Serial.println(netConfig.wifi_ssid);
    Serial.println(netConfig.wifi_pass);
    Serial.println(netConfig.mqtt_user);
    Serial.println(netConfig.mqtt_pass);
    Serial.println(netConfig.mqtt_server);
  }
}

void clearInputBuffer(char * inputBuffer) {
  // clear the inputBuffer buffer
  memset(inputBuffer, 0, sizeof(inputBuffer));
}
