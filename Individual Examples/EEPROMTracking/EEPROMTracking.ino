#include "ArduinoJson.h"
#include "StreamUtils.h"

const int BUTTON_PIN = 6;
const int LED_PIN = 13;
const int START_ADDRESS = 0;
const int DOC_SIZE = 200;
char stringBuffer [DOC_SIZE];
int buttonState = LOW;

StaticJsonDocument<DOC_SIZE> doc;
int eeprom_counter = 0;
char eeprom_char[6];
char* wifi_ap ;
char* wifi_pass;
char* mqtt_user;
char* mqtt_pass ;
char* mqtt_server;

void setup() {
  Serial.begin(9600);     // communication with the host computer
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);
  ReadEEPROM();
}

void loop() {

  delay(500);
  buttonState = digitalRead(BUTTON_PIN);
  PrintDocs();
  if (buttonState == HIGH) {
    doc["eeprom_counter"] = (int)doc["eeprom_counter"] + 1;
    doc["wifi_ap"] = "SantosProtones";
    doc["wifi_pass"] = "hombre.radioactivo";
    doc["mqtt_user"] = "homer";
    doc["mqtt_pass"] = "alohechopecho";
    doc["mqtt_server"] = "192.168.100.25";
    WriteEEPROM();
    ReadEEPROM();
  }
  else {
    // turn LED off:
    digitalWrite(LED_PIN, LOW);
  }


}

void PrintDocs() {
  Serial.print(F("[EEPROM] Config Saved to Memory "));
  Serial.println("Number of writes:");
  Serial.println (itoa(doc["eeprom_counter"], eeprom_char, 10));
  Serial.println ((char*)doc["wifi_ap"]);
  Serial.println ((char*)doc["wifi_pass"]);
  Serial.println ((char*)doc["mqtt_user"]);
  Serial.println ((char*)doc["mqtt_pass"]);
  Serial.println ((char*)doc["mqtt_server"]);

}

void WriteEEPROM() {
  //   To WRITE a JSON document to EEPROM, use:
  EepromStream eepromStream(START_ADDRESS, DOC_SIZE);
  serializeJson(doc, eepromStream);
}
void ReadEEPROM() {
  //To READ a JSON document to EEPROM, use:
  EepromStream eepromStream(START_ADDRESS, DOC_SIZE);
  DeserializationError error = deserializeJson(doc, eepromStream);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("[JSON PARSER] FAIL: "));
    Serial.println(error.f_str());
  }
  else {
    Serial.println(F("[JSON PARSER] SUCCESS"));
    Serial.println (itoa(doc["eeprom_counter"], eeprom_char, 10));
    Serial.println ((char*)doc["wifi_ap"]);
    Serial.println ((char*)doc["wifi_pass"]);
    Serial.println ((char*)doc["mqtt_user"]);
    Serial.println ((char*)doc["mqtt_pass"]);
    Serial.println ((char*)doc["mqtt_server"]);
  }
}
