0#include <TimerOne.h>
#include <WiFiEsp.h>
#include <WiFiEspClient.h>
#include <PubSubClient.h>
#include <SoftwareSerial.h>

const int STRING_LENGTH = 50;
const int TIMER_A = 10; //10ms
const int TIMER_B = 30000; //30s
const int TIMER_C = 10000; //10s
const int TIMER_ONE = 1000; //1ms
//********************* VARIABLE DECLARATIONS *****//
// WiFi Credentials
char wifi_ap [STRING_LENGTH]  = "SantosProtones";
char wifi_pass [STRING_LENGTH]  = "hombre.radioactivo";
int status = WL_IDLE_STATUS;

// MQTT Credentials
char mqtt_user [STRING_LENGTH] = "homer";
char mqtt_pass [STRING_LENGTH] = "alohechopecho";

// Name or IP Mosquitto Server
char mqtt_server[50] = "192.168.100.25";

// Variables
int temp = 0;
int hum = 0;
bool relay_01 = 0;
bool relay_02 = 0;

// Topics
char topic_humidity [STRING_LENGTH] = "homer/termostato/hum";
char topic_temperature [STRING_LENGTH] = "homer/temperature";
char topic_switch [STRING_LENGTH] = "homer/switch";

// Timers
struct Timer
{
  int currValue; //
  int endValue;  //
  boolean State; //
};

Timer TimerA;
Timer TimerB;
Timer TimerC;

// WiFi Client initialization
WiFiEspClient espClient;

// PubSubClient initialization
PubSubClient client(espClient);

//Virtual serial communication with ESP, it could be any two pins
//serial, pins 2: rx y 3: tx
SoftwareSerial wifiSerial(2, 3);

//**************** SETUP ********************************//

void setup() {
  // Serial Communication Log
  Serial.begin(9600);
  // WiFi init
  InitWiFi();
  ConfigMQTT();
  InitTimers();

}

//***************** MAIN LOOP ***************************//
void loop() {

  //Cada 10s hago el checkeo wifi
  if (TimerC.State == true)
  {
    //Check if WiFi is connected
    status = WiFi.status();
    if (status != WL_CONNECTED) {
      //Si falla la conexión, reconectamos el modulo
      ReconnectWifi();
    }
    //Check if MQTT connection is online
    if (!client.connected() ) {
      Serial.println("[MQTT OFFLINE] : Reconnecting");
      ReconnectMQTT();
      ConfigMQTT();
    }
    ResetTimerC();
  }

  for (int i = 0; i < 1000; i++) {
    client.loop();
    delay(1);
  }

  //Cada 30s publico los topics
  if (TimerB.State == true)
  {
    PublishTopics();
    ResetTimerB();
  }
}


//********************** MQTT *************************//
void PublishTopics()
{
  Serial.println("[MQTT PUBLISH]");
  // Prepare a JSON payload string
  String payload = "Copaduuuuuuuuuuu";

  // Send payload
  char attributes[100];
  payload.toCharArray( attributes, 100 );
  client.publish( topic_temperature , attributes );
  Serial.println( attributes );
  Serial.println("-----------------------");

}

void ReceiveTopics(char* topic, byte* payload, unsigned int length) {
  Serial.print("[MQTT SUBSCRIBE]");
  Serial.println(topic);
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  Serial.println("-----------------------");
}

void ReconnectMQTT() {
  //Creamos un loop en donde intentamos hacer la conexión
  Serial.print("[MQTT CONNECT] : ");
  Serial.println(mqtt_server);
  //si requiere usuario y contraseña la enviamos connect(clientId, username, password)
  String clientId = "ESP8266Client-" + String(random(0xffff), HEX);
  //   if (client.connect(clientId.c_str(), mqtt_user, mqtt_pass)) {
  if (client.connect(clientId.c_str())) {
    Serial.println("[MQTT DONE]");
  } else {
    Serial.println( "[MQTT FAILED] Client State: " );
    Serial.print( client.state() );
  }
}

void ConfigMQTT () {
  client.setServer(mqtt_server, 1883 );
  client.subscribe(topic_humidity);
  client.setCallback(ReceiveTopics);
}

//**************** WIFI ********************************//
void InitWiFi()
{
  //Inicializamos el puerto serial
  wifiSerial.begin(9600);
  //Iniciamos la conexión wifi
  WiFi.init(&wifiSerial);
  //Verificamos si se pudo realizar la conexión al wifi
  //si obtenemos un error, lo mostramos por log y denememos el programa
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("El modulo WiFi no esta presente");
    while (true);
  }
  ReconnectWifi();
}

void ReconnectWifi() {
  Serial.println("[WIFI CONNECT]");
  while (status != WL_CONNECTED) {
    Serial.print("Trying to connect with WPA SSID: ");
    Serial.println(wifi_ap);
    //Conectar a red WPA/WPA2
    status = WiFi.begin(wifi_ap, wifi_pass);
    delay(500);
  }
  Serial.println("[CONNECTED]");
}

//************** TIMERS *********************************//
void Timers()
{
  TimerA.currValue++;
  TimerB.currValue++;
  TimerC.currValue++;
  if (TimerA.currValue > TimerA.endValue)
  {
    TimerA.State = true;
  }
  if (TimerB.currValue > TimerB.endValue)
  {
    TimerB.State = true;
  }
  if (TimerC.currValue > TimerC.endValue)
  {
    TimerC.State = true;
  }
}

void InitTimers() {
  Timer1.initialize(TIMER_ONE); // 1 ms
  Timer1.attachInterrupt(Timers);

  TimerA.currValue = 0;
  TimerA.endValue = TIMER_A;
  TimerA.State = false;

  TimerB.currValue = 0;
  TimerB.endValue = TIMER_B;
  TimerB.State = false;

  TimerC.currValue = 0;
  TimerC.endValue = TIMER_C;
  TimerC.State = false;
}

void ResetTimerA() {
  TimerA.currValue = 0;
  TimerA.State = false;
}

void ResetTimerB() {
  TimerB.currValue = 0;
  TimerB.State = false;
}

void ResetTimerC() {
  TimerC.currValue = 0;
  TimerC.State = false;
}
