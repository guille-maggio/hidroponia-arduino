This project is part of a series of Apps which aim is to control a Hydroponic System with Arduino through WiFi.

This is the main app written in Cpp, there is also a Python interface to interact with the MCU through USB, and another one for Android written in java to configure the system via Bluetooth.

The Arduino connects to a MQTT server, ideally implemented with Mosquitto, and sends data about pH, TDS, water temperature, ambient temperature, and humidity.

You can configure all parameters through bluetooth (Android) o USB (Python) sending JSON objects with correct info.

All info on how to deploy is inside the wiki of this project:

https://gitlab.com/guille-maggio/hidroponia/-/wikis/home

